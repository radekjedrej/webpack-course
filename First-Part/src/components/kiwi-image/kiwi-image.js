import Edyta from './edyta.jpg'
import './kiwi-image.scss'

export default class KiwiImage {
  render() {
    const image = document.createElement('img')
    image.alt = "Kochanie"
    image.width = 300
    image.classList.add('edyta-image')
    image.src = Edyta
  
    const body = document.querySelector('body')
    body.appendChild(image)
  }
}

