import Edyta from './edyta.jpg'

export default function addImage() {
  const img = document.createElement('img')
  img.alt = "Kochanie"
  img.width = 300
  img.src = Edyta

  const body = document.querySelector('body')
  body.appendChild(img)

}